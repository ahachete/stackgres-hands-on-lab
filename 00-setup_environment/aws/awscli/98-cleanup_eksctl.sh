#!/bin/bash

source ./variables

eksctl delete cluster $K8S_CLUSTER_NAME --region $AWS_REGION

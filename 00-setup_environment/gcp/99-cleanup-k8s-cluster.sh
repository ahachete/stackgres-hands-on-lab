#!/bin/bash

source ./variables

gcloud container \
 --project ${GCP_PROJECT} clusters delete ${K8S_CLUSTER_NAME} \
 --region ${GCP_REGION}
